﻿using Goods.Web.Models.BaseClass;
using Goods.Web.Models.Goods;
using GoodsDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Goods.Web.Controllers
{
    public class GoodsController : BaseSecureController
    {          
        [HttpPost]
        public ActionResult bindSave(GoodsModel Model)
        {

            var bc = new GoodsBC();

            var productSave = bc.NewProduct(Model);
            return Json(new { objProNew = productSave });
        }
        [HttpPost]
        public ActionResult bindEdit(GoodsModel Model)
        {
            var bc = new GoodsBC();
            var productEdit = bc.EditProduct(Model);
            return Json(new { objProEdit = productEdit });
        }
        public ActionResult List()
        {
            var userstatus = new UserLoginModel();
            if (userstatus.MemberType==2)
            {
                return Redirect("ErrorPage/Error");
            }


            var member = UserStatus.MemberID; // Member-234
            var bc = new GoodsBC();
            var listProduct = bc.ShowList();
            ViewBag.listProduct = listProduct;

           // if (true) //check username password
           // {
                //set cookies
          //  }

            // check username , password 
            // query ข้อมูลที่จะใช้ใน Cookies มา
            //var model = new UserLoginModel();
            //model.UserName = "Admin";
            //model.MemberID = "Member-234";
            //model.EmpID = "CM59-111";
            //model.FirstName = "Mr.A";
            //model.LastName = "CCCC";
            //addCookies(model);
            

            return View();
        }
        public ActionResult New()
        {           
            var Product = new GoodsDetail();
            ViewBag.Product = Product;
            return View();
        }
        public ActionResult Edit(string id)
        {        
            var bc = new GoodsBC();
            var Product = bc.EditProductList(id);
            ViewBag.Product = Product;
            return View();
        }
        public ActionResult Detail(string id)
        {           
            var bc = new GoodsBC();
            var Product = bc.DetailProduct(id);
            ViewBag.Product = Product;
            return View();
        }
        [HttpPost]
        public ActionResult Delete(string ProductId)
        {
            var bc = new GoodsBC();
            var product = bc.DeleteProduct(ProductId);
            return Json(new { obj = product });
        }
    }
}