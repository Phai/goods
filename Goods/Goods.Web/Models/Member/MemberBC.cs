﻿using Goods.Web.Models.BaseClass;
using GoodsDatabase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Goods.Web.Models.Member
{
    public class MemberBC
    {
        public bool CheckUser(UserLoginModel model)
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<tdMember>("SELECT * FROM tdMember Where UserName ='" + model.UserName + "' AND Password ='" + model.Password + "'").ToList();
            if (list != null && list.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
        public tdMember GetMemberData(UserLoginModel model)
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<tdMember>("SELECT * FROM tdMember Where UserName ='" + model.UserName + "' AND Password ='" + model.Password + "'").ToList();
            var data = new tdMember();
            if (list != null && list.Count() > 0)
            { 
                data = list.First();
            }
            else
            {
                data = null;
            }
            return data;

        }
        public List<tdMember> ShowListMem()
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<tdMember>("SELECT * FROM tdMember").ToList();
            return list;
        }
        public bool DeleteMem(string MemberID)
        {
            var isResult = false;
            try
            {
                var qDB = new databaseContainer();
                var sql = "DELETE FROM tdMember WHERE MemberID='" + MemberID + "'";
                qDB.Database.ExecuteSqlCommand(sql);
                isResult = true;
            }
            catch (Exception)
            {
            }
            return isResult;
        }
        public UserLoginModel NewMember(UserLoginModel Model)
        {
            var qDB = new databaseContainer();
            // Insert - sql
            var NewMember = new tdMember();
            NewMember.MemberID = Model.MemberID;
            NewMember.FirstName = Model.FirstName;
            NewMember.LastName = Model.LastName;
            NewMember.Age = Convert.ToByte(Model.Age);
            NewMember.Email = Model.Email;
            NewMember.Phone = Model.Phone;
            NewMember.MemberType = Convert.ToByte(Model.MemberType);
            NewMember.UserName = Model.UserName;
            NewMember.Password = Model.Password;
            qDB.tdMembers.Add(NewMember);
            qDB.SaveChanges();
            return Model;
        }
        public tdMember EditMemList(string id)
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<tdMember>("SELECT * FROM tdMember Where MemberID ='" + id + "'").ToList();
            var data = new tdMember();
            if (list != null && list.Count() > 0)
            {
                data = list.First();
            }
            return data;
        }
        public UserLoginModel EditMem(UserLoginModel Model)
        {
            var qDB = new databaseContainer();
            var MemberData = qDB.Database.SqlQuery<tdMember>("SELECT * FROM tdMember Where MemberID = '" + Model.MemberID + "'").ToList();
            if (MemberData != null && MemberData.Count() > 0)
            {
                //Edit - By Models
                var Mem = MemberData.First();
                Mem.FirstName = Model.FirstName;
                Mem.LastName = Model.LastName;
                Mem.Age = Convert.ToByte(Model.Age);
                Mem.Email = Model.Email;
                Mem.Phone = Model.Phone;
                Mem.MemberType = Convert.ToByte(Model.MemberType);
                Mem.UserName = Model.UserName;
                Mem.Password = Model.Password;
                qDB.Entry(Mem).State = EntityState.Modified;
                qDB.SaveChanges();
            }
            
            return Model;
        }
        public tdMember DetailMem(string id)
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<tdMember>("SELECT * FROM tdMember Where MemberID ='" + id + "'").ToList();
            var data = new tdMember();
            if (list != null && list.Count() > 0)
            {
                data = list.First();
            }
            return data;
        }
    }
}