﻿$(function () {
    $(document).on("click", "#Save", function (e) {  // on .ใช้กับสคิปที่เกิดขึ้นทีหลัง
        if ($("#frm-goods").valid() && $("#frm-goods2").valid()) {

            $.ajax({
                url: "bindSave",
                data: {
                    ProductID: $("#ProductID").val(),
                    ProductName: $("#ProductName").val(),
                    Unit: $("#Unit").val(),
                    MarketingName: $("#MarketingName").val(),
                    Category: $("#Category").val(),
                    Costing: $("#Costing").val(),
                    BillingName: $("#BillingName").val(),
                    TypeOfTax: $("#TypeOfTax").val(),
                },
                type: "POST",
                success: function (data) {
                    if (data.objProNew != null && data.objProNew != undefined) {
                        console.log(data)
                        window.location = "List";
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
    });
    $(document).on("click", "#SaveEdit", function (e) {
        if ($("#frm-goods").valid() && $("#frm-goods2").valid()) {
            $.ajax({
                url: "bindEdit",
                data: {
                    ProductID: $("#ProductID").val(),
                    ProductName: $("#ProductName").val(),
                    Unit: $("#Unit").val(),
                    MarketingName: $("#MarketingName").val(),
                    Category: $("#Category").val(),
                    Costing: $("#Costing").val(),
                    BillingName: $("#BillingName").val(),
                    TypeOfTax: $("#TypeOfTax").val(),
                },
                type: "POST",
                success: function (data) {
                    if (data.objProEdit != null && data.objProEdit != undefined) {
                        console.log(data)
                        window.location = "Detail?id=" + data.objProEdit.ProductID;
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

    })
    $(document).on("click", "#GotoList", function (e) {
        window.location = "List";
    })
    $(document).on("click", "#gotoNew", function (e) {
        window.location = "New";
    })
})
function gotoEdit(id) {
    console.log(id);
    window.location = "Edit?id=" + id;
}
function gotoDetail(id) {
    console.log(id);

}
function gotoDelete(id) {
    console.log(id);
    var r = confirm("คุณต้องการลบใช่หรือไม่!!");
    if (r == true) {
        $.ajax({
            url: "Delete",
            data: {
                ProductId: id,
            },
            type: "POST",
            success: function (data) {
                if (data.obj != null && data.obj != undefined) {
                    console.log(data)
                    window.location = "List";
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }
    else {

    }
   
}
$.validator.setDefaults({
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$(function () {
    $("#frm-goods").validate({
        rules: {
            ProductID: "required",
            MarketingName: "required",
            BillingName: "required",
            ProductName: {
                required: true,
                maxlength: 10
            }
        },
        messages: {
            ProductID: "กรุณากรอกข้อมูล",
            MarketingName: "กรุณากรอกข้อมูล",
            BillingName: "กรุณากรอกข้อมูล",
            ProductName: {
                required: "กรุณากรอกข้อมูล",
                maxlength: "กรุณากรอกข้อมูลไม่เกิน 10 ตัวอักษร"
            }
        }
    });
});

$(function () {
    $("#frm-goods2").validate({
        rules: {
            Unit: "required",
            Category: "required",
            Costing: "required",
            TypeOfTax: "required",
          
        },
        messages: {
            Unit: "กรุณากรอกข้อมูล",
            Category: "กรุณากรอกข้อมูล",
            Costing: "กรุณากรอกข้อมูล",
            TypeOfTax: "กรุณากรอกข้อมูล",
        }
    });
});
