﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Goods.Web.Models.Goods
{
    public class GoodsModel
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public string MarketingName { get; set; }
        public string Category { get; set; }
        public string BillingName { get; set; }
        public string Costing { get; set; }
        public string TypeOfTax { get; set; }
    }
}