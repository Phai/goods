﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Goods.Web.Models.BaseClass
{
    public class UserLoginModel
    {
        public string UserName { get; set; }
        public int MemberID { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int MemberType { get; set; }

    }
}