﻿using Goods.Web.Models.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc
{
    public class BaseSecureController : Controller
    {
       
            public UserLoginModel UserStatus;
            public BaseSecureController()
            {
                getCookies();
            }
            public void addCookies(UserLoginModel model)
            {
                if (model != null)
                {
                    HttpCookie cookies = new HttpCookie("iBiz-Goods");
                    cookies.Values["UserName"] = model.UserName;
                    cookies.Values["MemberID"] = model.MemberID.ToString();
                    cookies.Values["Password"] = model.Password;
                    cookies.Values["FirstName"] = model.FirstName;
                    cookies.Values["LastName"] = model.LastName;
                    cookies.Values["Age"] = model.Age.ToString();
                    cookies.Values["Email"] = model.Email;
                    cookies.Values["Phone"] = model.Phone;
                    cookies.Values["MemberType"] = model.MemberType.ToString();

                    cookies.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookies);
                }
            }
            public void removeCookies()
            {
                HttpCookie cookies = new HttpCookie("iBiz-Goods");
                cookies.Value = null;
                cookies.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookies);
            }
            public void getCookies()
            {
                //get cookies เซ็ตค่าลง UserStatus
                UserStatus = new UserLoginModel();

                HttpCookie cookies = System.Web.HttpContext.Current.Request.Cookies["iBiz-Goods"];
                if (cookies != null)
                {
                    UserStatus.UserName = cookies["UserName"];
                    UserStatus.MemberID = Convert.ToInt32(cookies["MemberID"]);
                    UserStatus.Password = cookies["Password"];
                    UserStatus.FirstName = cookies["FirstName"];
                    UserStatus.LastName = cookies["LastName"];
                    UserStatus.Age = Convert.ToInt32(cookies["Age"]);
                    UserStatus.Email = cookies["Email"];
                    UserStatus.Phone = cookies["Phone"];
                    UserStatus.MemberType =Convert.ToInt32(cookies["MemberType"]);
                    ViewBag.Namelogin = UserStatus;                 
                   // UserStatus.EmpID = cookies["EmpID"];
                }
            }       
    }
}