﻿using Goods.Web.Models.BaseClass;
using Goods.Web.Models.Member;
using GoodsDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Goods.Web.Controllers
{
    public class MemberController : BaseSecureController
    { 
        public ActionResult Login(UserLoginModel model)
        { 
            return View();
        }

        [HttpPost]
        public ActionResult bindLogin(UserLoginModel model)
        {
           var bc =  new MemberBC();
           var isResult = bc.CheckUser(model);
           if (isResult) 
           {
               var MemData = bc.GetMemberData(model);
               if (MemData != null)
               {
                   var UserLogin = new UserLoginModel();
                   UserLogin.UserName = MemData.UserName;
                   UserLogin.MemberID = MemData.MemberID;
                   UserLogin.Password = MemData.Password;
                   UserLogin.FirstName = MemData.FirstName;
                   UserLogin.LastName =MemData.LastName;
                   UserLogin.Age =Convert.ToInt32(MemData.Age);
                   UserLogin.Email=MemData.Email;
                   UserLogin.Phone=MemData.Phone;
                   UserLogin.MemberType = Convert.ToInt32(MemData.MemberType);
                   addCookies(UserLogin);
                   isResult = true;
                }
           }
           else
           {
               // UserName , Password ผิดหรือไม่มี
               isResult = false;
           }

           return Json(new { IsResult = isResult });
        }

        public ActionResult ManageMember()
        {
            if (UserStatus.MemberType == 2)
            {
                return Redirect("~/ErrorPage/Error");
            }
            var member = UserStatus.MemberID; 
            var bc = new MemberBC();
            var listMember = bc.ShowListMem();
            ViewBag.listMember = listMember;
            return View();
        }
        
        public ActionResult EditMember(string id)
        {          
            if (UserStatus.MemberType == 2)
            {
                return Redirect("~/ErrorPage/Error");
            }
            var bc = new MemberBC();
            var Memdata = bc.EditMemList(id);
            ViewBag.Memdata = Memdata;
            return View();
        }
        public ActionResult AddMember()
        {          
            if (UserStatus.MemberType == 2)
            {
                return Redirect("~/ErrorPage/Error");
            }
            if (UserStatus.MemberType == 2)
            {
                return Redirect("ErrorPage/Error");
            }           
            var Memdata = new tdMember();
            ViewBag.Memdata = Memdata;
            return View();
        }
        public ActionResult DetailMember(string id)
        {;
            if (UserStatus.MemberType == 2)
            {
                return Redirect("~/ErrorPage/Error");
            }
            var bc = new MemberBC();
            var Memdata = bc.DetailMem(id);
            ViewBag.Memdata = Memdata;
            return View();
        }
        public ActionResult Logout()
        {
            removeCookies();
            return View("Login");
        }
        [HttpPost]
        public ActionResult bindSaveMem(UserLoginModel Model)
        {

            var bc = new MemberBC();

            var MemberSave = bc.NewMember(Model);
            return Json(new { objProNew = MemberSave });
        }

        [HttpPost]
        public ActionResult bindEditMem(UserLoginModel Model)
        {
            var bc = new MemberBC();
            var MemberEdit = bc.EditMem(Model);
            return Json(new { objMemEdit = MemberEdit });
        }


        [HttpPost]
        public ActionResult DeleteMember(string MemberID)
        {
            var bc = new MemberBC();
            var Memdata = bc.DeleteMem(MemberID);
            return Json(new { obj = Memdata });
        }
         
        
	}
}