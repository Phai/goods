﻿$(function () {
    $(document).on("click", "#AddMem", function (e) {  // on .ใช้กับสคิปที่เกิดขึ้นทีหลัง
        if ($("#MemberType1").is(":checked") == true) {
            var MemberType = 1;
        }
        else {
            var MemberType = 2;
        }
        if ($("#frm-Member").valid()) {
            $.ajax({
                url: "bindSaveMem",
                data: {
                    MemberID: $("#MemberID").val(),
                    FirstName: $("#FirstName").val(),
                    LastName: $("#LastName").val(),
                    Age: $("#Age").val(),
                    Email: $("#Email").val(),
                    Phone: $("#Phone").val(),
                    MemberType: MemberType,
                    UserName: $("#UserName").val(),
                    Password: $("#Password").val(),
                },
                type: "POST",
                success: function (data) {
                    if (data.objProNew != null && data.objProNew != undefined) {
                        console.log(data)
                        window.location = "ManageMember";
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
    });
    $(document).on("click", "#logout", function (e) {  
        window.location = "/Member/Logout";          
    })
    $(document).on("click", "#GotoManage", function (e) {
        window.location = "/Member/ManageMember";
    })
    $(document).on("click", "#login", function (e) {
        $.ajax({
            url: "bindLogin",
            data: {
                UserName: $("#UserName").val(),
                Password: $("#Password").val(),
                MemberID: $("MemberID").val(),
            },
            type: "POST",
            success: function (data) {
                if (data.IsResult) {
                    window.location = "/Goods/List";
                }
                else {
                    alert("UserName และ Password ไม่่ถูกต้อง กรุณาลองอีกครั้ง !!");
                }
            },
            error: function (e) {
                console.log(e);
            }
        })
    })
    $(document).on("click", "#SaveEditMem", function (e) {
        if ($("#MemberType1").is(":checked") == true) {
            var MemberType = 1;
        }
        else {
            var MemberType = 2;
        }
        if ($("#frm-Member").valid()) {
            $.ajax({
                url: "bindEditMem",
                data: {
                    MemberID: $("#MemberID").val(),
                    FirstName: $("#FirstName").val(),
                    LastName: $("#LastName").val(),
                    Age: $("#Age").val(),
                    Email: $("#Email").val(),
                    Phone: $("#Phone").val(),                   
                    MemberType: MemberType,
                    UserName: $("#UserName").val(),
                    Password: $("#Password").val(),
                },
                type: "POST",
                success: function (data) {
                    if (data.objMemEdit != null && data.objMemEdit != undefined) {
                        console.log(data)
                        window.location = "Detail?id=" + data.objMemEdit.MemberID;
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

    })
    $(document).on("click", "#gotoNewMem", function (e) {
        window.location = "AddMember";
    })
    $(document).on("click", ".Goods", function (e) {
        window.location = "/Goods/list";
    })
    $(document).on("click", ".Member", function (e) {
        window.location = "/Member/ManageMember";
    })
    $(document).on("click", "#BackToList", function (e) {
        window.location = "/Goods/list";
    })
})
function gotoEditMem(id) {
    console.log(id);
    window.location = "EditMember?id=" + id;
}


function gotoDeleteMem(id) {
    console.log(id);
    var r = confirm("คุณต้องการลบใช่หรือไม่!!");
    if (r == true) {
        $.ajax({
            url: "DeleteMember",
            data: {
                MemberID: id,
            },
            type: "POST",
            success: function (data) {
                if (data.obj != null && data.obj != undefined) {
                    console.log(data)
                    window.location = "ManageMember";
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }
    else {

    }

}