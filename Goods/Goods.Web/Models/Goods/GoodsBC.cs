﻿using GoodsDatabase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Goods.Web.Models.Goods
{
    public class GoodsBC
    {
        public GoodsModel NewProduct(GoodsModel Model)
        {
            var qDB = new databaseContainer();
            // Insert - sql
            var NewProduct = new GoodsDetail();
            NewProduct.ProductID = Model.ProductID;
            NewProduct.ProductName = Model.ProductName;
            NewProduct.Unit = Model.Unit;
            NewProduct.MarketingName = Model.MarketingName;
            NewProduct.Category = Model.Category;
            NewProduct.BillingName = Model.BillingName;
            NewProduct.Costing = Model.Costing;
            NewProduct.TypeOfTax = Model.TypeOfTax;
            qDB.GoodsDetails.Add(NewProduct);
            qDB.SaveChanges();
            return Model;
        }
        public List<GoodsDetail> ShowList()
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<GoodsDetail>("SELECT * FROM GoodsDetail").ToList();
            return list;
        }
        public GoodsDetail EditProductList(string id)
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<GoodsDetail>("SELECT * FROM GoodsDetail Where ProductID ='" + id + "'").ToList();
            var data = new GoodsDetail();
            if (list != null && list.Count() > 0)
            {
                data = list.First();
            }
            return data;
        }
        public GoodsModel EditProduct(GoodsModel Model)
        {
            var qDB = new databaseContainer();
            var ProductData = qDB.Database.SqlQuery<GoodsDetail>("SELECT * FROM GoodsDetail Where ProductID = '" + Model.ProductID + "'").ToList();
            if (ProductData != null && ProductData.Count() > 0)
            {
                //Edit - By Models
                var Pro = ProductData.First();
                Pro.ProductName = Model.ProductName;
                Pro.Unit = Model.Unit;
                Pro.MarketingName = Model.MarketingName;
                Pro.Category = Model.Category;
                Pro.BillingName = Model.BillingName;
                Pro.Costing = Model.Costing;
                Pro.TypeOfTax = Model.TypeOfTax;
                qDB.Entry(Pro).State = EntityState.Modified;
                qDB.SaveChanges();
            }
            // var qDB = new databaseContainer();
            // var sql = "UPDATE GoodsDetail SET ProductName = '" + Model.ProductName + "',Unit='" + Model.Unit + "',MarketingName='" + Model.MarketingName + "',Category='" + Model.Category + "',BillingName='" + Model.BillingName + "',Costing='" + Model.Costing + "',TypeOfTax='" + Model.TypeOfTax + "' WHERE ProductID ='" + Model.ProductID + "'";
            // qDB.Database.ExecuteSqlCommand(sql);
            return Model;
        }
        public GoodsDetail DetailProduct(string id)
        {
            var qDB = new databaseContainer();
            var list = qDB.Database.SqlQuery<GoodsDetail>("SELECT * FROM GoodsDetail Where ProductID ='" + id + "'").ToList();
            var data = new GoodsDetail();
            if (list != null && list.Count() > 0)
            {
                data = list.First();
            }
            return data;
        }

        public bool DeleteProduct(string ProductId)
        {
            var isResult = false;
            try
            {
                var qDB = new databaseContainer();
                var sql = "DELETE FROM GoodsDetail WHERE ProductID='" + ProductId + "'";
                qDB.Database.ExecuteSqlCommand(sql);
                isResult = true;
            }
            catch (Exception)
            {
            }
            return isResult;
        }
    }
}