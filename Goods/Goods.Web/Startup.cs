﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Goods.Web.Startup))]
namespace Goods.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
